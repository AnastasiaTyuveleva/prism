import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Home } from '../Home/home.jsx';
import { BrowserRouter as Redirect } from 'react-router-dom';
import './login.css';
import { connect } from 'react-redux';
import store from '../app/store.jsx'
const credentials = require('./credentials.json');

class Login_form extends React.Component {

	constructor(props) {
		super(props);
		this.checkCredentials = this.checkCredentials.bind(this);
		this.onClick = this.onClick.bind(this);
	}

	checkCredentials() {
		let user = document.getElementById('usernameInput').value;
		let passw = document.getElementById('passwordInput').value;

		let doCredentialsExist = Object.keys(credentials).some((key) => {
			return (user == key) && (passw == credentials[key]);
		})

		return doCredentialsExist;
	}

	onClick(e) {

		if (!this.checkCredentials()) {
			alert('Invalid username or password');

			e.preventDefault();
		}

		//pass username to store
		this.props.setUsername(document.getElementById('usernameInput').value);
	}

	render() {
		const SigninButton = () => {
			return (
				<button style={{ marginLeft: "130px" }}>
					<Link to="/home" onClick={this.onClick}>Sign in</Link>
				</button>
			)
		};

		return (
			<div>
				<nav className="navbar navbar-default">
					<div className="container-fluid">
						<div className="navbar-header">
							<a className="navbar-brand">Prism</a>
						</div>
					</div>
				</nav>

				<div className="wrapper">

					<form className="form-signin">
						<h2 className="form-signin-heading">Login</h2>
						<input type="text" className="form-control" name="username" defaultValue="Mia" placeholder="Username" required="" id="usernameInput" autoFocus="" />
						<input type="password" className="form-control" name="password" defaultValue="lovetwist" placeholder="Password" required="" id="passwordInput" />
						<label className="checkbox" style={{ marginLeft: "130px" }}>
							<input type="checkbox" style={{ marginLeft: "-15px" }} value="remember-me" id="rememberMe" name="rememberMe" />Remember me
					</label>
						<SigninButton />
					</form>
				</div>
			</div>
		)
	}
}

module.exports = connect(
	state => ({
		username: state.username
	}),
	dispatch => ({
		setUsername: (name) => {
			dispatch({
				type: 'SET_USERNAME',
				data: { username: name}
			})
		}
	})
)(Login_form);