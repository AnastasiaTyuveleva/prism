import React from 'react';
import { Link, BrowserRouter, Route } from 'react-router-dom';
import './myteam.css';
import MyteamGrid from './myteamGrid.jsx'
import { connect } from 'react-redux';
 
class Myteam extends React.Component{

    constructor(props) {
        super(props);
        this.Status = this.props.team.length ? '' : 'There are no people in your team...';
        this.removeTeammate = this.removeTeammate.bind(this);
    }

    //counter to be changed 
    removeTeammate() {
       this.setState({
           team: this.props.team,
           employees: this.props.employees
       })
    }

    // HEADER
    render(){
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">Prism</a>
                        </div>
                        <ul className="nav navbar-nav">
                            <li><a><Link to="/home">Home</Link></a></li>
                            <li><a><Link to="/people">People</Link></a></li>
                            <li className="active"><a><Link to="/myteam">My team ({this.props.team.length})</Link></a></li>
                            <li><a><Link to="/">Sign out</Link></a></li>
                        </ul>
                    </div>
                </nav>         
                <MyteamGrid removeTeammate={this.removeTeammate} />
                <div className="empty" style={{marginLeft: "40%"}}>{this.Status}</div>
            </div>
        )
    }
}

module.exports = connect(
    state => ({
        team: state.team,
        employees: state.employees
    })
)(Myteam);