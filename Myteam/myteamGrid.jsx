import React from 'react';
import Teammate from './teammate/teammate.jsx'
import { connect } from 'react-redux'; 

class MyteamGrid extends React.Component {

    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
        this.removeTeammate = this.removeTeammate.bind(this);
        this.props.modifyShowingTeam(this.props.team);
    }

    //counter to be changed
    removeTeammate() {
        this.setState({
            team: this.props.team,
            employees: this.props.employees
        })
        this.props.modifyShowingTeam(this.props.team);
        this.props.removeTeammate();
        document.getElementById('search').value = "";
    }

    search(event) {
        var searchQuery = event.target.value.toLowerCase();
        this.props.modifyShowingTeam(this.props.team.filter((person) => {
            let searchPerson = person.FirstName.toLowerCase() + ' ' + person.LastName.toLowerCase();
            return searchPerson.indexOf(searchQuery) !== -1;
        }));
    }

    render() {
        const self = this;
        const { removeTeammate } = this.props;
        let showingteam = this.props.showingTeam && this.props.showingTeam.slice() || [];
        return (
            <div>
                <input type="text" id="search" onChange={this.search} placeholder="Search"
                       style={{marginLeft: "40px", marginBottom: "30px", width: "400px" }} />
                <ul>
                    { 
                        showingteam.map(function (person) {
                            return <Teammate removeTeammate={self.removeTeammate} 
                                            employee={person} />
                        }) 
                    }
                </ul>
            </div>
        )
    }
}

module.exports = connect(
    state => ({
        team: state.team,
        employees: state.employees,
        showingTeam: state.showingTeam
    }),
    dispatch => ({
        modifyShowingTeam: (people) => {
            dispatch({
                type: 'CHANGE_TEAM',
                data: { showingTeam: people }
            })
        }
    })
)(MyteamGrid);