import React from 'react';
import './teammate.css';
import { connect } from 'react-redux';
import store from '../../app/store.jsx';

class Teammate extends React.Component {

    constructor(props) {
        super(props);
        this.remove = this.remove.bind(this);
    }

    remove() {
        //change button functionality and name
        this.props.employee.buttonProps = {
            name: 'Add to my team',
            func: undefined
        }

        //remove employee from team and modify store
        let index = this.props.team.map(person => {
            return person.photo
        }).indexOf(this.props.employee.photo);

        this.props.team.splice(index, 1);
        this.props.modifyTeam(this.props.team);

        //modify employee`s button properties
        index = this.props.employees.indexOf(this.props.employee);
        this.props.employees.splice(index, 1, this.props.employee);
        this.props.modifyEmployees(this.props.employees);

        this.setState({
            team: this.props.team,
            employees: this.props.employees
        })

        this.props.removeTeammate();
    }

    render() {
        const { removeTeammate, employee } = this.props;

        return (
            <div className="card" style={{ marginBottom: "30px", marginLeft: "40px", float: "left" }}>
                <img src={this.props.employee.photo} style={{ height: "125px", width: "125px", marginLeft: "20px", marginTop: "10px", float: "left" }} />
                <div className="card-body text-center" >
                    <h4 className="card-title" style={{ marginLeft: "140px" }}>{this.props.employee.FirstName + ' ' + this.props.employee.LastName}  </h4>
                    <br />
                    <br />
                    <div className="btn btn-primary" id="add" style={{ marginLeft: "140px", marginTop: "-60px" }} onClick={this.remove}>
                        Remove</div>
                </div>
            </div>
        )
    }
}

module.exports = connect(
    state => ({
        team: state.team,
        employees: state.employees
    }),
    dispatch => ({
        modifyTeam: (people) => {
            dispatch({
                type: 'CHANGE_TEAM',
                data: { team: people }
            })
        },
        modifyEmployees: (people) => {
            dispatch({
                type: 'CHANGE_EMPLOYEES_GRID',
                data: { employees: people }
            })
        }
    })
)(Teammate);