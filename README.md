BEFORE STARTING

You need all the node modules from package.json to be installed, so type the following command in terminal:
$ npm install

QUICK START

To start the application type the following command in terminal:
$ npm run dev

**To enter the application**, use one of five allowable credentials from Login/credentials.json file. 