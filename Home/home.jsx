import React from 'react';
import { connect } from 'react-redux';
import './home.css';
import { Link, BrowserRouter, Route } from 'react-router-dom';

class Home extends React.Component {

    constructor(props) {
        super(props);
        
    }

    render() {
        const helloName = 'Hello, ' + this.props.username;

        return (
            <div>               
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand">Prism</a>
                        </div>
                        <ul className="nav navbar-nav">
                            <li className="active"><a><Link to="/home">Home</Link></a></li>
                            <li><a><Link to="/people">People</Link></a></li>
                            <li><a><Link to="/myteam">My team ({this.props.team.length})</Link></a></li>
                            <li><a><Link to="/">Sign out</Link></a></li>
                        </ul>
                    </div>
                </nav>
                <div className="greeting">{helloName}</div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        username: state.username,
        team: state.team,
        employees: state.employees
    };
}

module.exports = connect(mapStateToProps)(Home);