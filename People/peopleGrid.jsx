import React from 'react';
import $ from 'jquery';
import PersonTemplate from './person/person.jsx';
import { connect } from 'react-redux';;

class PeopleGrid extends React.Component {

    constructor(props) {
        super(props);
        this.displayedEmployees = [];
        this.peopleArray = [];
        this.search = this.search.bind(this);
        this.getPeople = this.getPeople.bind(this);
        this.Execute = this.Execute.bind(this);
        this.removeTeammate = this.removeTeammate.bind(this);
        this.addTeammate = this.addTeammate.bind(this);
        this.Execute();
    }

    //counter to be changed
    addTeammate() {
        this.setState({
            employees: this.props.employees
        })
        this.props.modifyShowingEmployees(this.props.employees);
        this.props.changeCounter();
        document.getElementById('search').value = "";
    }

    //counter to be changed
    removeTeammate() {
        this.setState({
            employees: this.props.employees
        })
        this.props.modifyShowingEmployees(this.props.employees);
        this.props.changeCounter();
        document.getElementById('search').value = "";
    }

    search(event) {
        var searchQuery = event.target.value.toLowerCase();
        this.props.modifyShowingEmployees(this.peopleArray.filter((person) => {
            let searchPerson = person.FirstName.toLowerCase() + ' ' + person.LastName.toLowerCase();
            return searchPerson.indexOf(searchQuery) !== -1;
        }));
    }

    getPeople() {
        const self = this;
        return new Promise((resolve, reject) => {
            $.getJSON('http://prism.akvelon.net/api/employees/all', (data) => {
                for (let i = 0; i < data.length; i++) {
                    let personPhoto = 'http://prism.akvelon.net/api/system/getphoto/' + data[i].Id;
                    data[i].photo = personPhoto;
                    data[i].buttonProps = {
                        name: 'Add to my team',
                        func: undefined
                    }
                }
                this.props.addAllPeople(this.props.employees.length ? this.props.employees : data);
                this.props.modifyShowingEmployees(this.props.employees);
                this.peopleArray = data.slice();
                resolve(data);
            });
        })
    }

    Execute() {
        this.getPeople().then(response => {
        });
    }

    render() {
        const self = this;
        const { changeCounter } = this.props;
        return (
            <div>
                <input type="text" id="search" onChange={this.search} placeholder="Search"
                    style={{ marginLeft: "40px", marginBottom: "30px", width: "400px" }} />
                <ul>
                    {
                        this.props.showingEmployees.length &&
                        this.props.showingEmployees.map(function (person, i) {
                            return <PersonTemplate addTeammate={self.addTeammate}
                                removeTeammate={self.removeTeammate}
                                employee={person} />
                        })
                    }
                </ul>
            </div>
        )
    }
}

module.exports = connect(
    state => ({
        team: state.team,
        employees: state.employees,
        showingEmployees: state.showingEmployees
    }),
    dispatch => ({
        addAllPeople: (people) => {
            dispatch({
                type: 'CHANGE_EMPLOYEES_GRID',
                data: { employees: people }
            })
        },
        modifyShowingEmployees: (people) => {
            dispatch({
                type: 'CHANGE_EMPLOYEES_GRID',
                data: { showingEmployees: people }
            })
        }
    })
)(PeopleGrid);