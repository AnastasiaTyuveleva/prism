import React from 'react';
import ReactDOM from 'react-dom';
import { Link, BrowserRouter, Route } from 'react-router-dom';
import $ from 'jquery';
import PersonTemplate from './person/person.jsx';
import PeopleGrid from './peopleGrid.jsx';
import { connect } from 'react-redux';

class People extends React.Component {

    constructor(props) {
        super(props);
        this.changeCounter = this.changeCounter.bind(this);
    }

    //counter to be changed
    changeCounter() {
        this.setState({
            team: this.props.team,
            employees: this.props.employees
        })
    }

    // HEADER
    render() {
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">Prism</a>
                        </div>
                        <ul className="nav navbar-nav">
                            <li><a><Link to="/home">Home</Link></a></li>
                            <li className="active"><a><Link to="/people">People</Link></a></li>
                            <li><a><Link to="/myteam">My team ({this.props.team.length})</Link></a></li>
                            <li><a><Link to="/">Sign out</Link></a></li>
                        </ul>
                    </div>
                </nav>
                <PeopleGrid changeCounter={this.changeCounter} />
            </div>
        )
    }
}

module.exports = connect(
    state => ({
        team: state.team,
        employees: state.employees
    })
)(People);