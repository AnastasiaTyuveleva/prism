import React from 'react';
import './person.css';
import { connect } from 'react-redux';
import store from '../../app/store.jsx';

class PersonTemplate extends React.Component {

  constructor(props) {
    super(props);
    this.findTeammate = this.findTeammate.bind(this);
    this.remove = this.remove.bind(this);
    this.addToTeam = this.addToTeam.bind(this);
  }

  // if employee is already in the team 
  findTeammate() {
    for (let i = 0; i < this.props.team.length; i++) {
      let person = this.props.team[i];
      if (person.photo === this.props.employee.photo) {
        return true;
      }
    }
    return false;
  }

  remove() {
    this.props.employee.buttonProps = {
      name: 'Add to my team',
      func: this.addToTeam
    }

    //remove employee from team and modify store
    let team = this.props.team.slice();
    let index = team.indexOf(this.props.employee);
    team.splice(index, 1);
    this.props.modifyTeam(team);

    let employees = this.props.employees.slice();
    index = employees.indexOf(this.props.employee);
    employees.splice(index, 1, this.props.employee);
    this.props.modifyEmployees(employees);

    this.props.removeTeammate();

    this.setState({
      team: this.props.team,
      employees: this.props.employees
    })
  }

  addToTeam() {

    // if there is no such employee in the team
    if (!this.findTeammate()) {

      this.props.employee.buttonProps = {
        name: 'Remove',
        func: this.remove
      }

      //remove employee from team and modify store
      let team = this.props.team.slice();
      team.push(this.props.employee);
      this.props.modifyTeam(team);

      let employees = this.props.employees.slice();
      let index = employees.indexOf(this.props.employee);
      employees.splice(index, 1, this.props.employee);
      this.props.modifyEmployees(employees);

      this.props.addTeammate();

      this.setState({
        team: this.props.team,
        employees: this.props.employees
      })
    }

  }

  render() {
    const { addTeammate, removeTeammate, employee } = this.props;
    return (
      <div className="card" style={{ marginBottom: "30px", marginLeft: "40px", float: "left" }}>
        <img src={this.props.employee.photo} style={{ height: "125px", width: "125px", marginLeft: "20px", marginTop: "10px", float: "left" }} />
        <div className="card-body text-center" >
          <h4 className="card-title" style={{ marginLeft: "140px" }}>{this.props.employee.FirstName + ' ' + this.props.employee.LastName}  </h4>
          <br />
          <br />
          {
            <div className="btn btn-primary" id={this.props.employee.Id} style={{ marginLeft: "140px", marginTop: "-60px" }}
              onClick={this.props.employee.buttonProps.func || this.addToTeam}>
              {this.props.employee.buttonProps.name} </div>
          }

        </div>
      </div>
    )
  }
}

module.exports = connect(
  state => ({
    team: state.team,
    employees: state.employees
  }),
  dispatch => ({
    modifyEmployees: (people) => {
      dispatch({
        type: 'CHANGE_EMPLOYEES_GRID',
        data: { employees: people }
      })
    },
    modifyTeam: (people) => {
      dispatch({
        type: 'CHANGE_TEAM',
        data: { team: people }
      })
    }
  })
)(PersonTemplate);