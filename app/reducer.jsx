let reducer = function (state, action) {

    switch (action.type) {
        case "SET_USERNAME":
            return Object.assign({}, state, action.data);
        case "CHANGE_TEAM":
            return Object.assign({}, state, action.data);
        case "CHANGE_EMPLOYEES_GRID":
            return Object.assign({}, state, action.data);
        default:
            return state || { username: "", team: [], employees: [], showingTeam: [], showingEmployees: [] };
    }
    return Object.assign({}, state,
        {
            username: state.username,
            team: state.team,
            employees: state.employees,
            showingTeam: state.showingTeam,
            showingEmployees: state.showingEmployees,
        });
}

module.exports = reducer;