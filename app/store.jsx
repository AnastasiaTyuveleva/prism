import { createStore } from 'redux';

let reducer = require('./reducer.jsx');
let store = createStore(reducer);

export default store;