import ReactDOM from 'react-dom';
import React from 'react';
import { BrowserRouter as Router, Route, Switch, Red } from 'react-router-dom';
import { browserHistory } from 'react-router';
import Login from './Login/login.jsx';
import Home from './Home/home.jsx'
import People from './People/people.jsx';
import Myteam from './Myteam/myteam.jsx';
import { Link, BrowserRouter } from 'react-router-dom';
import store from './app/store.jsx'
import { Provider } from 'react-redux'

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/home" component={Home} />
          <Route path="/people" component={People} />
          <Route path="/myteam" component={Myteam} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  document.getElementById("app")
) 
